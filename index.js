const express = require("express");
const bodyParser = require("body-parser");
const hbs = require("express-hbs");

const app = express();
const routes = require("./routes");

const PORT = process.env.PORT || 3000;

app.engine(
  "hbs",
  hbs.express4({
    partialsDir: __dirname + "/views/partials",
  })
);
app.set("view engine", "hbs");
app.set("views", __dirname + "/views");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/", routes);

app.listen(PORT, () => console.log(`Listening on port ${PORT}...`));
