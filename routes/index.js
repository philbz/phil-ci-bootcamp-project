const express = require("express");
const router = express.Router();

const homeController = require("../controllers/homeController");
const squareController = require("../controllers/squareController");

router.get("/", homeController.getHome);

router.get("/square", squareController.getSquare);

module.exports = router;
