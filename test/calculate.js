const calculate = require("../lib/calculate");

const chai = require("chai");
const should = chai.should();

describe("Calculate", function () {
  describe("#square()", function () {
    it("should return 9 for input of 3", function () {
      calculate.square(3).should.equal(9);
    });
    it("should return 81 for input of 9", function () {
      calculate.square(9).should.equal(81);
    });
    it("should return 100 for input of 10", function () {
      calculate.square(10).should.equal(100);
    });
    it("should properly parse a string input", function () {
      calculate.square("6").should.equal(36);
    });
  });
});
