const calculate = require("../lib/calculate");

exports.getSquare = (req, res) => {
  const square = calculate.square(req.query.n);
  res.render("square", { square });
};
